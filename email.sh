#!/bin/bash 
### Usage:
# Sends stdin to an email, takes subject as an argument.
# i.e. :
# uptime | ./test.sh "Uptime"
# To, From, and SMTP host/port are burned into the script
# via variables defined below. 
# Can be installed/linked within $PATH, 
# and named something sensible like `notify-admin`  

### Variables - Change these to fit your needs
from="[Originating Email Address]"
name="`hostname`"
to="[Destination Email Address]"
subject=$1 

### Script - Do not change below this line 
headers="\
From: $name <$from>
To: $to
Subject: $subject
Date: `date` 
" 

message="$(< /dev/stdin)" 
combined="$headers$message" 

curl \
    --url smtp://[hostname]:[port] \
    --ssl-reqd \
    --netrc-file ./netrc \
    --mail-from $from \
    --mail-rcpt $to \
    --upload-file - \
    -s -S\
    <<< $combined
