# curl-smtp

Send Email from terminal

Takes `subject` as argument, accepts message body via `stdin`

### Usage:

`echo "[Message Body]" | ./email.sh "[Subject]"`

`echo "This is a test" | ./email.sh "Testing!"` 

# Variables to Set

### email.sh

- `[Originating Email Address]` - Usually MUST match the SMTP username with most services. 
- `[Destination Email Address]` - The email address to send the message to
- `[hostname]` - The SMTP server being used to send the message
- `[port]` - Port for SMTP - 25, 465, 587

### netsh
- `[SMTP Hostname]` - The SMTP server hostname again, used to lookup the user/pass
- `[username]` - The SMTP Username - Usually your originating email address
- `[password]` - the SMTP Password
